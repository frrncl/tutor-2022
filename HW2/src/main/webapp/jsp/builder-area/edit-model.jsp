<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
       <meta charset="utf-8">
       <title>edit model</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../../css/template.css">
        <link rel="stylesheet" href="../../css/font-awesome-4.7.0/css/font-awesome.min.css">
        <script src="../../js/utils.js"></script>
       <script src="../../js/edit-model-page.js"></script>
    </head>
    <body>
        <div id="navbar-area"></div>

        <div id="sidebar-area"></div>

        <div class="container-fluid text-center" id="wrapper">
            <c:choose>
                <c:when test="${model!=null}">
                    <h1>Edit Model: "${model.name}"</h1>


                    <form method="POST" action="<c:url value="/model/update/"/>">
                        <input type="hidden" id="original_name" name="original_name" value="${model.name}"/>
                        <label for="description">description:</label>
                        <input name="description" type="text" value="${model.description}"/><br/><br/>
                        <button type="submit">Submit</button><br/>
                    </form>
                    <button id="delete-button">Delete</button><br/>
                </c:when>
                <c:otherwise>
                    <h1>Insert new model</h1>


                    <form method="POST" action="<c:url value="/model/insert/"/>">
                        <label for="name">model name:</label>
                        <input name="name" type="text"/><br/><br/>
                        <label for="description">description:</label>
                        <input name="description" type="text"/><br/><br/>
                        <button type="submit">Submit</button><br/>

                    </form>

                </c:otherwise>
            </c:choose>


            <c:choose>
                <c:when test="${message.error}">
                    <p><c:out value="${message.message}"/></p>
                </c:when>
                <c:otherwise></c:otherwise>
            </c:choose>
        </div>
        <div id="footer-area"></div>
    </body>
</html>