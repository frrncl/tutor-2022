<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>register</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/template.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="${pageContext.request.contextPath}/js/utils.js"></script>
    </head>
    <body>
        <div id="navbar-area"></div>

        <div id="sidebar-area"></div>


        <div class="container-fluid text-center" id="wrapper">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/jsp/homepage.jsp">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Register</li>
                </ol>
            </nav>
            <h1>Register</h1>
            <form method="POST" action="<c:url value="/user/register/"/>">

                <fieldset class="form-group">
                    <div class="form-group row">
                        <div class="col-sm-4 offset-4">
                            <input type="text" class="form-control" name="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 offset-4">
                            <input type="text" class="form-control" name="first_name" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 offset-4">
                            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-4 offset-4">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-4 offset-4">
                            <input type="password" class="form-control" name="rpassword" placeholder="Repeat Password">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-4 offset-4">
                            <c:if test="${message.error}">
                                <p><c:out value="${message.message}"/></p>
                            </c:if>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-4 offset-4">
                            <button type="submit" class="btn btn-primary">Sign in</button>
                        </div>
                    </div>
                </div>
            </form>

            <c:choose>
                <c:when test="${message.error}">
                    <p><c:out value="${message.message}"/></p>
                </c:when>
                <c:otherwise></c:otherwise>
            </c:choose>
        </div>
        <div id="footer-area"></div>
    </body>
</html>