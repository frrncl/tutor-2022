// self executing function here
$(document).ready( function(event) {
    // your page initialization code here
    // the DOM will be available here
    //alert("dom ready!");
    getHomepageContent();
});


function getHomepageContent() {

    $.ajax({
        url: new URL(contextPath+'/park/overview/'),
        method: 'GET',
        success: createOverviewPage,
        fail: function(data){
                      console.log(data);
                      alert("problem processing the request");
                  }
    })

}

function createOverviewPage(data){
    var jsonData = data;
    var parks = jsonData['data'];

    ncards = 3;

    var hpcontent = "";
    for(let i=0; i<parks.length; i++){
        var pname = sanitize(parks[i]['name'])
        hpcontent += "<div class='row'>";
        hpcontent += "<div class='col-12'>";
        hpcontent += "<h1>"+pname+"</h1>";
        hpcontent += "<p>email: "+sanitize(parks[i]['email'])+"</p>";
        hpcontent += "<p>address: "+sanitize(parks[i]['address'])+"</p>";

        hpcontent += "<div id='park-"+pname+"-carousel' class='carousel slide carousel-multi-item' data-ride='carousel'>";

        //Controls
        if(parks[i]['rides'].length>ncards){
            hpcontent += "  <div class='controls-top'>";
            hpcontent += "      <a class='btn-floating' href='#park-"+pname+"-carousel' data-slide='prev'><i class='fa fa-chevron-left'></i></a>";
            hpcontent += "      <a class='btn-floating' href='#park-"+pname+"-carousel' data-slide='next'><i class='fa fa-chevron-right'></i></a>";
            hpcontent += "  </div>";
        }


        for(let j=0; j<parks[i]['rides'].length; j++){
            if(j===0){
                hpcontent += "<div class='carousel-item active'>";
                hpcontent += "<div class='row'>";
            } else if(j%ncards===0){
                hpcontent += "<div class='carousel-item'>";
                hpcontent += "<div class='row'>";
            }


            const figures = {
                'DropTower': 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/16-doubleshot.jpg/175px-16-doubleshot.jpg',
                'Carousel': 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Man%C3%A8geLR1.jpg/175px-Man%C3%A8geLR1.jpg',
                'Coaster': 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Luna_Park_Melbourne_scenic_railway.jpg/175px-Luna_Park_Melbourne_scenic_railway.jpg',
                'PendulumRide': 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Jambore.JPG/175px-Jambore.JPG',
                'WaterRide': 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/PirateFalls.JPG/175px-PirateFalls.JPG'
            };

            hpcontent+="<div class='col-4' style='float:left'>";
            hpcontent+="    <div class='card mb-2'>";
            hpcontent+="        <img class='card-img-top' src='"+figures[parks[i]['rides'][j]['model']]+"' alt='Card image cap'>";
            hpcontent+="        <div class='card-body text-left'>";
            hpcontent+="            <h4 class='card-title'>Ride "+(j+1)+"</h4>";
            hpcontent+="            <p class='card-text'>ride description: "+sanitize(parks[i]['rides'][j]['description'])+"<br/>model description: "+sanitize(parks[i]['rides'][j]['model_desc'])+"</p>";
            hpcontent+="        </div>";
            hpcontent+="    </div>";
            hpcontent+="</div>";
            if(j%ncards===(ncards-1)||j===parks[i]['rides'].length-1){
                hpcontent += "</div>";
                hpcontent += "</div>";
            }
        }

        hpcontent +="</div>";

        hpcontent += "</div>";
        hpcontent += "</div>";
    }

    $("#homepage-content").html(hpcontent);

}
