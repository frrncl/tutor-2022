var contextPath = 'http://127.0.0.1:8080/HW2-2022-1.0';


function sanitize(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}


$(document).ready(loadTemplate());

function loadTemplate(){

    var loggedIn = sessionStorage.getItem("loggedIn");
    var userEmail = sessionStorage.getItem("userEmail");
    var userAuthorization = sessionStorage.getItem("userRole");

    definizioneRichiesta = {
        url: new URL(contextPath+'/html/reusable-snippets/navbar.html'),
        method: 'GET',
        success: function(data){

            $("#navbar-area").html(data);
            $("#logo-button").attr("href", contextPath+"/jsp/homepage.jsp");
            $("#logout-button").attr("href", contextPath+"/user/logout/");
            $("#login-button").attr("href", contextPath+"/jsp/login.jsp");
            $("#register-button").attr("href", contextPath+"/jsp/register.jsp");


            if(loggedIn){
                $('.logged').addClass('d-block').removeClass('d-none');
                $('.unlogged').addClass('d-none').removeClass('d-block');
                $("#user-email").html(userEmail);
            }
            if(!loggedIn){
                $('.logged').addClass('d-none').removeClass('d-block');
                $('.unlogged').addClass('d-block').removeClass('d-none');
            }
        },
        fail: function(data){
            console.log(data);
            alert("problem processing the request");
        }
    }
    //load navbar
    $.ajax(definizioneRichiesta);


    //load sidebar
    $.ajax({
        url: new URL(contextPath+'/html/reusable-snippets/sidebar.html'),
        method: 'GET',
        success: function(data){
            $("#sidebar-area").html(data);
            $("#homepage-link-button").attr("href", contextPath+"/jsp/homepage.jsp");
            $("#session-link-button").attr("href", contextPath+"/html/maintainer-area/insert-session-file.html");
            $("#edit-maintenance-link-button").attr("href", contextPath+"/html/maintainer-area/edit-maintenance.html");
            $("#search-maintenance-link-button").attr("href", contextPath+"/html/maintainer-area/search-maintenance.html");
            $("#builder-link-button").attr("href", contextPath+"/html/builder-area/builder-page.html");
            $("#admin-link-button").attr("href", contextPath+"/html/admin-area/admin-page.html");
            if(loggedIn){
                if (userAuthorization=='maintainer' || userAuthorization=='builder' || userAuthorization=='admin'){
                    $('.maintainer').addClass('d-block').removeClass('d-none');
                    if (userAuthorization=='builder' || userAuthorization=='admin'){
                        $('.builder').addClass('d-block').removeClass('d-none');
                        if (userAuthorization =='admin'){
                            $('.admin').addClass('d-block').removeClass('d-none');
                        }
                    }
                }
            } if(!loggedIn) {
                $('.maintainer').addClass('d-none').removeClass('d-block');
                $('.builder').addClass('d-none').removeClass('d-block');
                $('.admin').addClass('d-none').removeClass('d-block');
            }

            $("#drop-down-maintainer-menu").on("click", function(){

                if ($("#icon-maintainer-menu").hasClass("fa-angle-down")){
                    $('#icon-maintainer-menu').addClass('fa-angle-up').removeClass('fa-angle-down');
                } else{
                    $('#icon-maintainer-menu').addClass('fa-angle-down').removeClass('fa-angle-up');

                }
            });

        },
        fail: function(data){
            console.log(data);
            alert("problem processing the request");
        }
    });


    //load footer
    $.ajax({
        url: new URL(contextPath+'/html/reusable-snippets/footer.html'),
        method: 'GET',
        success: function(data){
                $("#footer-area").html(data);
            },
        fail: function(data){
              console.log(data);
              alert("problem processing the request");
          }
    });

}

