INSERT INTO amupark.park VALUES
('AcroparkNY','info@acroparkny.com', 'Via Gradenigo, Padova'),
('AquaFun','public@aquafun.com', 'Corso Milano, Vicenza'),
('RideAdventures','customerservice@rideadventures.com', 'Via Parco della Vittoria, Monopoli');


INSERT INTO amupark.account VALUES 
('guglielmo.faggioli@parkville.com', md5('myPassword'), 'Guglielmo', 'Faggioli', 'admin'),
('John@acroparkny.com', md5('JonhPassword'), 'John', 'Smith', 'maintainer'),
('Paul@acroparkny.com', md5('PaulPassword'), 'Paul', 'Black', 'builder');

INSERT INTO amupark.model VALUES 
('TowerLift', 'TowerLift Model, 2010, 8 seats, Max load 2000 Kg, link: www.amusementRides.com/rides/Towerlift'),
('Coaster', 'Coaster Model, 2002, 10 double seats (max 20 people), Max load 5000 Kg, link: www.amusementRides.com/rides/Coaster');


INSERT INTO amupark.ride(description, parkid, modelid) VALUES
('Kids Tower lift in Acropark - Installed in 2010', 'AcroparkNY', 'TowerLift'),
('Adults Roller coaster in RideAdventures - Installed in 2015', 'RideAdventures', 'Coaster'),
('Adults Tower lift in AquaFun - Installed in 2015', 'AquaFun', 'TowerLift'),
('Kids Roller coaster in AquaFun - Installed in 2018', 'AquaFun', 'Coaster');


INSERT INTO amupark.device(name, description, type, rideid) VALUES
('dyn1', 'brakets - tower lift', 'dynamometer', 1),
('bar1', 'brakets - tower lift', 'barometer', 1),
('cap1', 'front row - coaster', 'capacitor', 2),
('cap2', 'back row - coaster', 'capacitor', 2),
('termo1', 'upper - tower lift', 'termometer', 3),
('bar1', 'lower - tower lift', 'barometer', 3),
('hygro1', 'front row - coaster', 'hygrometer', 4),
('cap1', 'back row - coaster', 'dynamometer', 4);
