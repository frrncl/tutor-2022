package rest;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.sql.DataSource;


import java.sql.Connection;

public class RestResource {

    protected final HttpServletRequest req;
    protected final HttpServletResponse res;
    protected final Logger logger;
    protected final DataSource ds;
    public RestResource(HttpServletRequest req, HttpServletResponse res, DataSource ds){
        this.req = req;
        this.res = res;
        this.ds = ds;
        this.logger = LogManager.getLogger(this.getClass());
    }

}
