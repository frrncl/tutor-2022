package rest;

import dao.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.*;
import resource.*;
import utils.ErrorCode;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class DeviceRestResource extends RestResource {

    protected final String op;
    protected ErrorCode ec = null;
    protected String response = null;
    protected final String[] tokens;

    public DeviceRestResource(HttpServletRequest req, HttpServletResponse res, DataSource ds) {
        super(req, res, ds);
        op = req.getRequestURI();
        tokens = op.split("/");

    }

    public void getRideDevices() throws IOException {
        try {
            List<Device> devices = new GetDevicesByRideDatabase(ds.getConnection(),  new Ride(Integer.parseInt(tokens[5]))).getDevicesByRide();
            ec = ErrorCode.OK;
            response = new JSONObject().put("data", new JSONObject().put("devices-list", devices)).toString();

        } catch (SQLException e){
            initError(ErrorCode.INTERNAL_ERROR);
            logger.error("stacktrace:", e);
        } finally { respond(); }
    }

    public void insertRideDevices() throws IOException {
        try {
            if (new GetRideByIdDatabase(ds.getConnection(), new Ride(Integer.parseInt(tokens[5]))).getRideById() == null) {
                initError(ErrorCode.RIDE_NOT_FOUND);
            } else {

                List<Device> devices = Device.fromJSONlist(req.getInputStream());
                List<JSONObject> errors = new ArrayList<>();
                for (Device device : devices) {
                    if (new InsertDeviceDatabase(ds.getConnection(), device).insertDevice() == null) {
                        if (new GetRideByIdDatabase(ds.getConnection(), new Ride(device.getRideid())).getRideById()==null) {
                            errors.add(new JSONObject().put("reason", "rideid not found").put("device", device));
                        } else  {
                            errors.add(new JSONObject().put("reason", "unknown").put("device", device));
                        }
                    }
                }
                if (errors.size() > 0) {
                    response = new JSONObject().put("total", devices.size()).put("failed", errors.size()).put("errors", errors).toString();
                } else {
                    ec = ErrorCode.OK;
                    response = new JSONObject().put("result", "successful").toString();
                }
            }
        } catch (JSONException e) {
            initError(ErrorCode.BADLY_FORMATTED_JSON);
            logger.error("stacktrace:", e);
        } catch (SQLException  e){
            logger.error("stacktrace:", e);
            initError(ErrorCode.INTERNAL_ERROR);
        } finally { respond(); }

    }


    public void deleteRideDevices() throws IOException {
        try {
            Ride ride = new Ride(Integer.parseInt(tokens[5]));

            if (new GetRideByIdDatabase(ds.getConnection(), ride).getRideById()  != null) {

                List<Device> devicesList = new GetDevicesByRideDatabase(ds.getConnection() ,ride).getDevicesByRide();
                List<JSONObject> errors = new ArrayList<>();


                for (Device device : devicesList) {
                    if (new DeleteDeviceDatabase(ds.getConnection(), device).deleteDevice() == null) {
                        if (new GetRideByIdDatabase(ds.getConnection(), ride).getRideById() == null) {
                            errors.add(new JSONObject().put("deviceid", device).put("reason", "rideid not found"));
                        } else {
                            errors.add(new JSONObject().put("deviceid", device).put("reason", "unknown"));
                        }
                    }
                }

                if (errors.size()>0) {
                    response = new JSONObject().put("total", devicesList.size()).put("failed", errors.size()).put("errors", errors).toString();
                } else {
                    ec = ErrorCode.OK;
                    response = new JSONObject().put("result", "successfull").toString();
                }

            } else {
                initError(ErrorCode.RIDE_NOT_FOUND);
            }
        }  catch (SQLException e){
            initError(ErrorCode.INTERNAL_ERROR);
            logger.error("stacktrace:", e);
        } finally { respond(); }

    }

    public void getDevice() throws IOException {
        try {
            Device device = new GetDeviceByIdDatabase(ds.getConnection(), new Device(Integer.parseInt(tokens[4]))).getDeviceById();
            if (device!=null){
                res.setStatus(HttpServletResponse.SC_OK);
                response = new JSONObject().put("data", new JSONObject().put("device", device.toJSON())).toString();
            } else {
                initError(ErrorCode.DEVICE_NOT_FOUND);
            }

        } catch (SQLException e){
            initError(ErrorCode.INTERNAL_ERROR);
            logger.error("stacktrace:", e);
        } finally { respond(); }
    }

    public void updateDevice() throws IOException {
        try {
            int deviceid = Integer.parseInt(tokens[4]);
            Device device = Device.fromJSON(req.getInputStream());
            if (deviceid != device.getDeviceid()) {

            } else {
                if (new UpdateDeviceDatabase(ds.getConnection(), device).updateDevice() != null) {
                    ec = ErrorCode.OK;
                    response = new JSONObject().put("result", "successfull").toString();
                } else {
                    if (new GetDeviceByIdDatabase(ds.getConnection(), device).getDeviceById() == null) {
                        initError(ErrorCode.DEVICE_NOT_FOUND);
                    } else if (new GetRideByIdDatabase(ds.getConnection(), new Ride(device.getRideid())).getRideById() == null) {
                        initError(ErrorCode.RIDE_NOT_FOUND);
                    } else {
                        initError(ErrorCode.INTERNAL_ERROR);
                    }
                }
            }
        } catch (JSONException e) {
            initError(ErrorCode.BADLY_FORMATTED_JSON);
            logger.error("stacktrace:", e);
        } catch (SQLException e) {
            initError(ErrorCode.INTERNAL_ERROR);
            logger.error("stacktrace:", e);
        } finally { respond(); }
    }

    public void insertDevice() throws IOException {
        try {
            Device device = Device.fromJSON(req.getInputStream());
            if (new InsertDeviceDatabase(ds.getConnection(), device).insertDevice()!= null) {
                ec = ErrorCode.OK;
                response = new JSONObject().put("result", "successfull").toString();
            } else if (new GetRideByIdDatabase(ds.getConnection(), new Ride(device.getRideid())).getRideById()==null) {
                initError(ErrorCode.RIDE_NOT_FOUND);
            } else {
                initError(ErrorCode.INTERNAL_ERROR);
            }
        } catch (JSONException e) {
            initError(ErrorCode.BADLY_FORMATTED_JSON);
            logger.error("stacktrace:", e);
        } catch (SQLException e){
            initError(ErrorCode.INTERNAL_ERROR);
            logger.error("stacktrace:", e);
        } finally { respond(); }
    }

    public void deleteDevice() throws IOException {
        try {
            if (new DeleteDeviceDatabase(ds.getConnection(), new Device(Integer.parseInt(tokens[4]))).deleteDevice()!=null){
                ec = ErrorCode.OK;
                response = new JSONObject().put("result", "successful").toString();
            } else {
                initError(ErrorCode.DEVICE_NOT_FOUND);
            }
        } catch (SQLException e){
            initError(ErrorCode.INTERNAL_ERROR);
            logger.error("stacktrace:", e);
        } finally { respond(); }
    }

    private void respond() throws IOException {
        res.setStatus(ec.getHTTPCode());
        res.getWriter().write(response);
    }

    private void initError(ErrorCode ec){
        this.ec = ec;
        response = ec.toJSON().toString();
    }
}
